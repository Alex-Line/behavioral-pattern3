package com.iteco.alexline.visitor.graphic;

import com.iteco.alexline.visitor.visitor.Visitor;
import lombok.Getter;

@Getter
public class Circle extends AbstractGraphic {

    private final String name = "Circle";

    private final Double radius;

    public Circle(Double radius) {
        this.radius = radius;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public Double getArea() {
        return radius * radius * Math.PI;
    }

    @Override
    public Double getPerimeter() {
        return 2 * radius * Math.PI;
    }

    @Override
    public void rotate() {
        System.out.println("Какая разница, он ведь круглый");
    }

}