package com.iteco.alexline.visitor.graphic;

import com.iteco.alexline.visitor.visitor.Visitor;
import lombok.Getter;

@Getter
public class Triangle extends AbstractGraphic {

    private final String name = "90 degrees Triangle";

    private final Double a;

    private final Double b;

    public Triangle(Double a, Double b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public Double getArea() {
        return a * b / 2;
    }

    @Override
    public Double getPerimeter() {
        return a + b + Math.sqrt(a * a + b * b);
    }

    @Override
    public void rotate() {
        System.out.println("Triangle is rotating");
    }

}