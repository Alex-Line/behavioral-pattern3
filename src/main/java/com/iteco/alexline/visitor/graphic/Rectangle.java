package com.iteco.alexline.visitor.graphic;

import com.iteco.alexline.visitor.visitor.Visitor;
import lombok.Getter;

@Getter
public class Rectangle extends AbstractGraphic {

    private final String name = "Rectangle";

    private final Double a;

    private final Double b;

    public Rectangle(Double a, Double b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public Double getArea() {
        return a * b;
    }

    @Override
    public Double getPerimeter() {
        return 2 * (a + b);
    }

    @Override
    public void rotate() {
        System.out.println("Rectangle is rotating");
    }

}