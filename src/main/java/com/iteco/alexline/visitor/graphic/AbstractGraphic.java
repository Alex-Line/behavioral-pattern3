package com.iteco.alexline.visitor.graphic;

import com.iteco.alexline.visitor.visitor.Visitor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class AbstractGraphic {

    private final String name = "Abstract graphic";

    public abstract void accept(final Visitor visitor);

    public abstract Double getArea();

    public abstract Double getPerimeter();

    public abstract void rotate();

}