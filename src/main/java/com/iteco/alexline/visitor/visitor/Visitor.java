package com.iteco.alexline.visitor.visitor;

import com.iteco.alexline.visitor.graphic.Circle;
import com.iteco.alexline.visitor.graphic.Rectangle;
import com.iteco.alexline.visitor.graphic.Triangle;

public interface Visitor {

    void visit(Circle circle);

    void visit(Triangle triangle);

    void visit(Rectangle rectangle);

}