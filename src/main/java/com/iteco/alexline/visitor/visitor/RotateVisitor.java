package com.iteco.alexline.visitor.visitor;

import com.iteco.alexline.visitor.graphic.Circle;
import com.iteco.alexline.visitor.graphic.Rectangle;
import com.iteco.alexline.visitor.graphic.Triangle;

public class RotateVisitor implements Visitor {

    @Override
    public void visit(Circle circle) {
        circle.rotate();
    }

    @Override
    public void visit(Triangle triangle) {
        triangle.rotate();
    }

    @Override
    public void visit(Rectangle rectangle) {
        rectangle.rotate();
    }

}