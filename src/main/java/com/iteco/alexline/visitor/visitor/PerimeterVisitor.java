package com.iteco.alexline.visitor.visitor;

import com.iteco.alexline.visitor.graphic.Circle;
import com.iteco.alexline.visitor.graphic.Rectangle;
import com.iteco.alexline.visitor.graphic.Triangle;

public class PerimeterVisitor implements Visitor {

    @Override
    public void visit(Circle circle) {
        System.out.println("Circle perimeter is " + circle.getPerimeter());
    }

    @Override
    public void visit(Triangle triangle) {
        System.out.println("Triangle perimeter is " + triangle.getPerimeter());
    }

    @Override
    public void visit(Rectangle rectangle) {
        System.out.println("Rectangle perimeter is " + rectangle.getPerimeter());
    }

}