package com.iteco.alexline.visitor.visitor;

import com.iteco.alexline.visitor.graphic.Circle;
import com.iteco.alexline.visitor.graphic.Rectangle;
import com.iteco.alexline.visitor.graphic.Triangle;

public class DrawingVisitor implements Visitor {

    @Override
    public void visit(Circle circle) {
        System.out.println("Draws " + circle.getName());
    }

    @Override
    public void visit(Triangle triangle) {
        System.out.println("Draws " + triangle.getName());
    }

    @Override
    public void visit(Rectangle rectangle) {
        System.out.println("Draws " + rectangle.getName());
    }

}