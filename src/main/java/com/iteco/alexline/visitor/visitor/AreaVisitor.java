package com.iteco.alexline.visitor.visitor;

import com.iteco.alexline.visitor.graphic.Circle;
import com.iteco.alexline.visitor.graphic.Rectangle;
import com.iteco.alexline.visitor.graphic.Triangle;

public class AreaVisitor implements Visitor {

    @Override
    public void visit(Circle circle) {
        System.out.println("Area of graphic is " + circle.getArea());
    }

    @Override
    public void visit(Triangle triangle) {
        System.out.println("Area of graphic is " + triangle.getArea());
    }

    @Override
    public void visit(Rectangle rectangle) {
        System.out.println("Area of graphic is " + rectangle.getArea());
    }

}