package com.iteco.alexline.visitor;

import com.iteco.alexline.visitor.graphic.AbstractGraphic;
import com.iteco.alexline.visitor.graphic.Circle;
import com.iteco.alexline.visitor.graphic.Rectangle;
import com.iteco.alexline.visitor.graphic.Triangle;
import com.iteco.alexline.visitor.visitor.*;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<AbstractGraphic> graphics = new ArrayList<>();
        graphics.add(new Circle(15.0));
        graphics.add(new Triangle(3.0, 4.0));
        graphics.add(new Rectangle(5.0, 5.0));

        List<Visitor> visitors = new ArrayList<>();
        visitors.add(new DrawingVisitor());
        visitors.add(new AreaVisitor());
        visitors.add(new PerimeterVisitor());
        visitors.add(new RotateVisitor());

        graphics.forEach(graphic -> {
            System.out.println("- - - - - - - - - - - - - - - - - - - - -");
            visitors.forEach(graphic::accept);
        });
    }

}